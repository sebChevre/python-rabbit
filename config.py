class Config:
    def __init__(self):
        
        self.user        = 'sandbox'
        self.password    = 'sandbox'
        self.host        = '10.28.20.222'
        self.port        = 5672
        self.vhost       = '/'
        self.exchange     = ''
        self.nbMessage    = 10
        self.delay =        0 
        self.queue  =      ''
        self.topic =       ''

def defaultValues() :
    config = Config()
    return config

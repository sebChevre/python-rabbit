#!/usr/bin/env python
from ast import arg
import queue
import pika
import datetime
import time
import argparse
import config
cfg = config.defaultValues()

connection  = ''
channel     = ''

def localISO8601Date():
    return datetime.datetime.now().isoformat()

def log(msg):
     print("[" + localISO8601Date() + "] - " + msg)

def parseAndSetArguments ():
    global cfg

    parser = argparse.ArgumentParser()
    #obligatoire
    parser.add_argument('-n', required=True, type=int, help='[integer] Nombre de messages')
    parser.add_argument('-d', required=True, type=int, help='[integer] Delay entre chaque messages en secondes')
   
    
    #optionnel
    parser.add_argument('-vh',  const = cfg.vhost, default = cfg.vhost, nargs='?' ,help=f'[string] Vhost name, default value {cfg.host}')
    parser.add_argument('-e',   nargs='?' ,help=f'[string] Exchnage, default value is xch-[queueOrTopicName]-d(direct) or -f(fanout)')

    parser.add_argument('-u',  const = cfg.user, default = cfg.user, nargs='?' ,help=f'[string] user name, default value {cfg.user}')
    parser.add_argument('-p',  const = cfg.password, default = cfg.password, nargs='?' ,help=f'[string] password, default value: ****')

    parser.add_argument('-q', nargs='?', help='[string] Nom de la queue')
    parser.add_argument('-t', nargs='?', help='[string] Nom du topic')

    
    args = parser.parse_args()
    
    log(f"nbMessage args: {args.n}")
    cfg.nbMessage = args.n
    
    log(f"delay args: {args.d}")
    cfg.delay = args.d

    log(f"queue args: {args.q}")
    cfg.queue = args.q

    log(f"vhost args: {args.vh}")
    cfg.vhost = args.vh

    log(f"user args: {args.u}")
    cfg.user = args.u

    log(f"password args: *****")
    cfg.password = args.p
    
    defineQueueOrTopicName (args.q, args.t, parser)

    defineExchangeName(args.e)

def defineQueueOrTopicName (queue, topic, parser) :
    if queue is None and topic is None:
        parser.error("At least queue (-q) or topic (-t) musst be defined")
    elif (queue is not None and topic is not None):
        log("queue and topic are defined, queue will be used")
        cfg.queue = queue
    elif queue is not None:
        log(f"queue args: {queue}")
        cfg.queue = queue
    elif topic is not None:
        log(f"queue args: {topic}")
        cfg.topic = topic 

def defineExchangeName (exchange) :
    
    if not cfg.queue:
        exchangeName = cfg.topic
    else :
        exchangeName = cfg.queue

    if exchange is None :
        log(f"exchange value args empty. Exchange with queue name: xch-{exchangeName}-d'")
        cfg.exchange = f'xch-{exchangeName}-d'
    else:
        log(f"exchange args: {exchange}")
        cfg.exchange = f'xch-{exchange}-d'

def createConnection () :
    global connection

    credentials = pika.PlainCredentials(cfg.user, cfg.password)
    parameters = pika.ConnectionParameters(cfg.host, cfg.port, cfg.vhost, credentials)
    connection = pika.BlockingConnection(parameters)

def createQueueOrTopic () :

    if not cfg.queue :
        createTopic ()
    else :
        createQueue ()

def createQueue () :
    global channel
    
    channel = connection.channel()
    channel.exchange_declare(exchange = cfg.exchange, exchange_type='direct')
    channel.queue_declare(queue = cfg.queue)
    channel.queue_bind(exchange = cfg.exchange, queue = cfg.queue, routing_key='')

def createTopic () :
    global channel

    channel = connection.channel()
    channel.exchange_declare(exchange=cfg.exchange, exchange_type='fanout')

def main () :
    
    parseAndSetArguments ()
    createConnection ()
    createQueueOrTopic ()
    
    counter = 0

    while (cfg.nbMessage > counter) :
        channel.basic_publish(exchange=cfg.exchange, routing_key='', body='Hello World!')
        print(" [x] Sent 'Hello World!'")
        counter += 1
        time.sleep(cfg.delay)

    connection.close()

if __name__ == "__main__":
    main()